const product_page_url = 'https://www.bnn.in.th/th/p/apple/apple-ipad'
const privacy_button = '.btn-group > .-primary'
const shopping_cart_button = '.btn-shopping-cart > .icon'
const qty = '#input25476'
const increase_button = '#increase25476 > .icon'
const decrease_button = '#decrease25476 > .icon'
const total_product_price = '.price-container .price'
const delete_button = '.btn-delete'
const text_after_delete_cart = '.cart-product-empty-container'
const cart_image = '.image > img'

//Add product to shopping cart
beforeEach(() => {
  cy.visit(product_page_url)
  cy.url().should('eq', product_page_url)
  //accepted privacy
  cy.get(privacy_button).click()
  //add product to shopping cart
  cy.get('.product-list').contains('Apple iPad Mini 6 (2021) Wi-Fi + Cellular 8.3 inch').click()
  cy.contains('หยิบใส่ตะกร้า').click()
})

describe('Adjust shopping cart', () => {
  //ignore uncaught error
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })
  it('User can increase/decrease quantity of product', () => {
    cy.visit(Cypress.config('baseUrl'))
    cy.get(shopping_cart_button).click()
    //verify quantity of product before adjust quantity
    cy.get(qty).should('have.value', '1')
    //verify total price before adjust quantity
    cy.get(total_product_price).contains('฿23,400')
    //increase quantity of product
    cy.get(increase_button).click()
    //verify quantity of product after increase quantity
    cy.get(qty).should('have.value', '2')
    //verify total price after increase quantity
    cy.get(total_product_price).contains('฿46,800')
    // increase quantity of product
    cy.get(decrease_button).click()
    //verify quantity of product after decrease quantity
    cy.get(qty).should('have.value', '1')
    //verify total price after decrease quantity
    cy.get(total_product_price).contains('฿23,400')

  })
  it('User can deleted product', () => {
    cy.visit(Cypress.config('baseUrl'))
    cy.get(shopping_cart_button).click()
    //verify quantity of product before adjust quantity
    cy.get(qty).should('have.value', '1')
    //verify total price before adjust quantity
    cy.get(total_product_price).contains('฿23,400')
    //deleted product
    cy.get(delete_button).click()
    //verify message after deleted product
    cy.get(text_after_delete_cart).contains('ไม่มีสินค้าในตะกร้า').should('contain.text', 'ไม่มีสินค้าในตะกร้า')
    cy.get(text_after_delete_cart).contains('คุณไม่มีสินค้าในตะกร้า โปรดเลือกหยิบสินค้าที่ต้องการซื้อลงตะกร้า').should('contain.text', 'คุณไม่มีสินค้าในตะกร้า โปรดเลือกหยิบสินค้าที่ต้องการซื้อลงตะกร้า')
    cy.get(cart_image).should('be.visible')

  })
})